from pymongo import MongoClient

def connect_db():
    """
    Connect to Mongo database and return db handle
    """
    ip = "localhost"
    port = 27017
    client = MongoClient(ip, port)

    db = client.timemaster
    return db


def empty_types():
    """
    Empty timemaster.types collection.
    """
    db = connect_db()
    types = db.types
    types.drop()


def insert_types(dict):
    """
    Insert types to timemaster.types collection.
    :param dict: a dict of type dict from aTimeLogger
    """
    empty_types()

    db = connect_db()
    types = db.types
    for key, value in dict.items():
        empty_children_value = value
        empty_children_value['children'] = []
        dict[key] = empty_children_value

    for key, value in dict.items():
        if value['parent'] != None:
            parent_id = next(iter(value['parent'].values()))
            # modify parent value
            dict[key]['parent'] = parent_id
            # add children info
            parent_children_value = dict[parent_id]['children']
            parent_children_value.append(key)
            dict[parent_id]['children'] = parent_children_value

    for key, value in dict.items():
        temp = {"_id": key,
                "name": value['name'],
                "order": value['order'],
                "deleted": value['deleted'],
                "revision": value['revision'],
                "group": value['group'],
                "parent": value['parent'],
                "children": value['children']}
        types.insert_one(temp)


def isEmpty_intervals():
    """
    Return whether timemaster.intervals is empty
    """
    db = connect_db()
    intervals = db.intervals
    return intervals.count({}) == 0


def empty_intervals():
    """
    Empty timemaster.intervals collection.
    """
    db = connect_db()
    intervals = db.intervals
    intervals.drop()


def insert_new_intervals(list):
    """
    Insert new intervals to timestamp.intervals
    :param list: a list for new intervals
    interval sample:
    [{'userGuid': None, 'comment': '', 'from': 1550814614, 'activityGuid': '3e3caad6-058e-4f51-80e4-8ba923ecbd2f', 'to': 1550814643, 'revision': None, 'type': {'guid': 'da3632d3-f559-4280-a447-7cd1b881d047'}, 'guid': '8e28c249-d46a-4514-98e7-7bdf06a45839'}]

    """
    db = connect_db()
    intervals = db.intervals
    for interval in list:
        temp = {"_id": interval['activityGuid'],
                "type": interval['type']['guid'],
                "from": interval['from'],
                "to": interval['to']}

        intervals.insert_one(temp)

def get_last_interval_endtime():
    """
    Get the end time of the last interval in the database.
    """
    db = connect_db()
    intervals = db.intervals
    cursor = last_interval = intervals.find().sort([("to", -1)]).limit(1)
    return list(cursor)[0]["to"]

def get_db_intervals(t_start, t_end):
    """
    Get the intervals within the query period in the database.
    """
    db = connect_db()
    intervals = db.intervals
    cursor = intervals.find({"$and":
                                [{"to": {"$gt": t_start}},
                                {"to": {"$lt": t_end}}]
                            })
    return list(cursor)

if __name__ == "__main__":
    pass
