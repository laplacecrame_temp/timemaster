"""This module operates the aTimelogger API"""

import requests
import json
from datetime import datetime, timedelta, timezone
from db import get_last_interval_endtime, isEmpty_intervals

def get_time_token():
    """
    Return the access token for aTimelogger.
    Reference: http://blog.timetrack.io/rest-api/
    """


    user, pwd = [line.rstrip('\n') for line in open('config/pwd.txt')]
    params = {'username': user, 'password':pwd, 'grant_type': 'password'}
    url = "https://app.atimelogger.com/oauth/token"
    r = requests.post(url, params = params, auth = ('androidClient', 'secret'))
    return json.loads(r.text)['access_token']

def get_types(token):
    """
    Retuen the dict of types arrow{guid: type}
    :params: token: access token from aTimelogger
    :return: a list of types
    """
    url = "https://app.atimelogger.com/api/v2/types"
    r = requests.get(url, headers = {'Authorization': "bearer"+token})
    types = json.loads(r.text)['types']
    type_dict = {}
    for item in types:
        type_dict[item['guid']] = item

    return type_dict

def get_new_intervals(token):
    """
    Return new activity history from last recorded time to now
    :params: token: access token to request data
    :return a list of intervals
    """

    t_end = datetime.now()
    NEW_DAYS = 1
    t_start = t_end - timedelta(days = NEW_DAYS)
    t_end = str(round(t_end.timestamp()))
    t_start = str(round(t_start.timestamp()))
    if not isEmpty_intervals():
        t_start = get_last_interval_endtime()

    url = "https://app.atimelogger.com/api/v2/intervals"
    r = requests.get(url,
                    params = {'from': t_start, 'to': t_end},
                    headers = {'Authorization': "bearer"+token})
    intervals = json.loads(r.text)['intervals']

    return intervals


if __name__ == '__main__':
    pass
