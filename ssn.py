from twython import Twython

def tw(tw_msg, img_file):
    with open("pwd.txt", 'r') as f:
        APP_KEY = f.readline()[:-1]
        APP_SECRET = f.readline()[:-1]
        OAUTH_TOKEN = f.readline()[:-1]
        OAUTH_TOKEN_SECRET = f.readline()[:-1]

    twitter = Twython(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
    photo = open(img_file, 'rb')
    response = twitter.upload_media(media=photo)
    twitter.update_status(status=tw_msg, media_ids=[response['media_id']])

if __name__ == '__main__':
    pass
