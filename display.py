import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime, timedelta

def stats_aggregate(types, intervals):
    fig, ax = plt.subplots()
    line_width = 0.3
    cmap = plt.get_cmap("RdYlGn")

    pie_type = []
    interval_len = []
    type_len = []
    for interval in intervals:
        length = interval['to']-interval['from']
        print(type(length))
        if interval['type'] not in pie_type:
            pie_type.append(interval['type'])
            interval_len.append([length])
            type_len.append(length)
        else:
            interval_len[pie_type.index(interval['type'])].append(length)
            type_len[pie_type.index(interval['type'])] += length

    print(interval_len)

    temp = 0
    inner_color = []
    outer_color = []
    inner_list = []
    for item in interval_len:
        item.sort()
        inner_list += item
        temp_len = len(item)
        inner_color += range(temp+1, temp+temp_len+1)
        outer_color.append(temp+temp_len+1)
        temp += (3 + temp_len)

    inner_color = np.array(inner_color)/temp
    outer_color = np.array(outer_color)/temp
    ax.pie(type_len, radius=1, colors=cmap(outer_color),
           labels = [types[item]['name'] for item in pie_type],
           autopct = lambda t: str(timedelta(seconds=int(t/100*np.sum(np.array(type_len))))),
           wedgeprops=dict(width=line_width, edgecolor='w'))
    # ax.legend()
    ax.pie(inner_list, radius=1-line_width, colors=cmap(inner_color), wedgeprops=dict(width=line_width, edgecolor='w'))

    fig.savefig("pie.png")
    print(outer_color)
    print(inner_color)


if __name__ == "__main__":
    pass
